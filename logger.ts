import _ from 'lodash';
import winston, { format, transports } from 'winston';
const {
  combine,
  timestamp,
  printf,
  splat,
  simple,
  json,
  colorize
} = format;
const logFormat = winston.format.printf(function(info) {
    return `${info.level}: ${info.message} ${JSON.stringify(_.omit(info, ['message', 'level']), null, 4)}\n`;
  });
// Create a Bunyan logger that streams to Cloud Logging
// Logs will be written to: "projects/YOUR_PROJECT_ID/logs/bunyan_log"
const logger = winston.createLogger({
  format: combine(
    timestamp({ format: 'YYYY-MM-DD[T]HH:mm:ss.SSSZ' }),
    format.json()
  ),
  handleExceptions: true,
  level: 'verbose',
  transports: [
    // loki,
    new transports.Console({
      format: format.combine(format.colorize(), logFormat),
    }),
    new transports.File({
      level: 'error',
      filename: 'logs/error.log'
    }),
    new transports.File({
      filename: 'logs/info.log'
    }),
    new transports.File({
      level: 'verbose',
      filename: 'logs/verbose.log'
    })
  ],
  exceptionHandlers: [
    new transports.File({
      filename: 'logs/exceptions.log'
    }),
  ]
});


export default logger;