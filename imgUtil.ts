/* eslint-disable require-jsdoc */
import { spawn } from 'child_process';
import * as _ from 'lodash';
import { Readable } from 'stream';

export function getInfo(img: Buffer): Promise<any> {
  return new Promise((resolve, reject): void => {
    let res = '';
    const proc = spawn(
      `convert`,
      ['-', 'json:-'],
    );
    Readable.from(img).pipe(proc.stdin, { end: true });
    proc.stdout.on('data', (data) => {
      res += data.toString();
    });
    proc.on('error', reject);

    proc.on('close', (code) => {
      if (code !== 0) {
        return reject(new Error(`convert process exited with code ${code}`));
      }
      res = JSON.parse(res)[0].image;
      _.unset(res, 'properties');
      return resolve(res);
    });
  });
}

export function convert(img: Buffer): Promise<Buffer> {
  return new Promise((resolve, reject): void => {
    const bufs: Buffer[] = [];
    const proc = spawn(
      'convert',
      [
        '-',
        '-strip',
        '-quality', '90',
        'jpg:-',
      ],
    );
    Readable.from(img).pipe(proc.stdin, { end: true });
    proc.stdout.on('data', (data) => {
      bufs.push(data);
    });
    proc.on('error', reject);

    proc.on('close', (code) => {
      if (code !== 0) {
        return reject(new Error(`convert process exited with code ${code}`));
      }
      return resolve(Buffer.concat(bufs));
    });
  });
}

export function compress(img: Buffer): Promise<Buffer> {
  return new Promise((resolve, reject): void => {
    const bufs: Buffer[] = [];
    const proc = spawn(
      'convert',
      [
        '-',
        '-strip',
        '-quality', '90',
        'webp:-',
      ],
    );
    Readable.from(img).pipe(proc.stdin, { end: true });
    proc.stdout.on('data', (data) => {
      bufs.push(data);
    });
    proc.on('error', reject);

    proc.on('close', (code) => {
      if (code !== 0) {
        return reject(new Error(`convert process exited with code ${code}`));
      }
      return resolve(Buffer.concat(bufs));
    });
  });
}

export function getInfoStream(img: Readable): Promise<{ info: any, img: Buffer }> {
  return new Promise((resolve, reject): void => {
    let res = '';
    const proc = spawn(
      `convert`,
      ['-', 'json:-'],
    );
    var bufs = [];
    img.on('data', function (d) {
      proc.stdin.write(d);
      bufs.push(d);
    });
    img.on('end', function () {
      proc.stdin.end();
    })
    // img.pipe(proc.stdin, { end: true });
    proc.stdout.on('data', (data) => {
      res += data.toString();
    });
    proc.on('error', reject);

    proc.on('close', (code) => {
      if (code !== 0) {
        return reject(new Error(`convert process exited with code ${code}`));
      }
      var buf = Buffer.concat(bufs);

      res = JSON.parse(res)[0].image;
      _.unset(res, 'properties');
      return resolve({ info: res, img: buf });
    });
  });
}

export function convertStream(img: Readable, format: string): Promise<Buffer> {
  return new Promise((resolve, reject): void => {
    const bufs: Buffer[] = [];
    const proc = spawn(
      'convert',
      [
        '-',
        '-strip',
        '-quality', '90',
        'jpg:-',
      ],
    );
    img.pipe(proc.stdin, { end: true });
    proc.stdout.on('data', (data) => {
      bufs.push(data);
    });
    proc.on('error', reject);

    proc.on('close', (code) => {
      if (code !== 0) {
        return reject(new Error(`convert process exited with code ${code}`));
      }
      return resolve(Buffer.concat(bufs));
    });
  });
}
