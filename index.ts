import { exec as execSync } from "child_process";
import FormData from "form-data"
import got, { CancelableRequest } from "got"
import { performance } from "perf_hooks";
import { promisify } from "util";
import logger from "./logger";
import Oled from 'rpi-oled';
import font from 'oled-font-5x7';
import IORedis from "ioredis";
import { nanoid } from "nanoid";
import { Response } from "got";
import { result } from "lodash";
import { compress, convert } from "./imgUtil";
import { Rekognition } from "aws-sdk";

const rek = new Rekognition({
    region: 'eu-central-1',
    credentials: {
        accessKeyId: 'AKIAUS5BJRXEWD4CJW5Y',
        secretAccessKey: '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'
    }
});

const exec = promisify(execSync);
const redis = new IORedis("rediss://:5206e5334c7647e8b9d04b578f23b009@eu1-wired-hog-33433.upstash.io:33433");

const boundary = `---${nanoid()}`;
const api = got.extend({
    prefixUrl: "https://api-us.faceplusplus.com",
    method: 'POST', dnsCache: true, http2: true, resolveBodyOnly: false,
    headers: { 'Content-Type': 'multipart/form-data; boundary=' + boundary }
});


var opts = {
    width: 128,
    height: 64,
};

var oled = new Oled(opts);
oled.turnOnDisplay();

async function takePic(): Promise<Buffer> {
    const { stdout, stderr } = await exec('raspistill -n -e bmp -t 5000 -o -', { encoding: 'buffer', maxBuffer: Math.pow(10, 6) * 64 });
    return stdout;
}

function drawStats(face: any, id: string) {
    const { attributes } = face;

    oled.clearDisplay();
    oled.update();

    oled.setCursor(1, 55);
    oled.writeString(font, 1, 'ID: ' + id, 1, true);

    oled.setCursor(1, 1);
    oled.writeString(font, 1, attributes.gender.value + ', ' + attributes.age.value, 1, true);

    oled.setCursor(1, 10);
    oled.writeString(font, 1, 'Gender: ' + attributes.gender.value, 1, true);

    oled.setCursor(1, 20);
    oled.writeString(font, 1, 'Bty', 1, true);
    oled.drawRect(27, 21, 100, 6, 1);
    let len = Math.floor((attributes.beauty.female_score / 100) * 100);
    oled.fillRect(27, 21, len, 6, 1);

    oled.setCursor(1, 30);
    oled.writeString(font, 1, 'Skin', 1, true);
    oled.drawRect(27, 31, 100, 6, 1);
    len = Math.floor((attributes.skinstatus.health / 100) * 100);
    oled.fillRect(27, 31, len, 6, 1);

    oled.update();
}

async function detectFaces(pic, id) {
    const form = new FormData();
    form.setBoundary(boundary);

    form.append("api_key", process.env.api_key);
    form.append("api_secret", process.env.api_secret);

    form.append("return_attributes", 'gender,age,beauty,skinstatus,eyegaze,emotion,smiling');
    form.append("image_file", await convert(pic), { filename: 'img.jpg' });

    const body = Buffer.from(form.getBuffer());

    const [detect, skinanalyze] = await Promise.all([api("facepp/v3/detect", { body }), api("facepp/v1/skinanalyze", { body })]);

    logger.info('parsed image', { detect: detect.timings, skin: skinanalyze.timings });
    await redis.hmset('result:' + id, ['detect', detect.body], ['skin', skinanalyze.body]);
    await redis.hmset('timings:' + id, ['detect', detect.timings], ['skin', skinanalyze.timings]);

    const data = JSON.parse(detect.body);
    const skinData = JSON.parse(skinanalyze.body);

    logger.verbose('image data', { detect: data, skin: skinData });

    if (data.face_num > 0) {
        const [face] = data.faces;
        drawStats(face, id);
    }
}

async function main() {
    const id = nanoid(8);
    await redis.multi().lpush('pics', id).hset('times', [id, Math.floor(Date.now() / 1000)]).exec();
    const pic = await takePic();
    await redis.setBuffer('img:' + id, await compress(pic));

    const {Labels} = await rek.detectLabels({ Image: { Bytes: await convert(pic) } }).promise();
    const {TextDetections} = await rek.detectText({ Image: { Bytes: await convert(pic) } }).promise();
    logger.info('scanned image', {id});

    await redis.hmset('result:' + id, ['labels', JSON.stringify(Labels)], ['text', JSON.stringify(TextDetections)]);

    oled.clearDisplay();
    oled.update();

    oled.setCursor(1, 1);
    oled.writeString(font, 1, Labels.map(l => l.Name).join(', '), 1, true);

    setImmediate(main);
}

main();